package com.cs.childalert;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class Checker extends IntentService {
    private int i;
    private Handler handler = new Handler();
    public Checker() {
        super("Checker");
        // TODO Auto-generated constructor stub
    }


    @Override
    protected void onHandleIntent(Intent intent) {


        while(true){
            if(isMyServiceRunning( Services.class, getBaseContext() )){
                Log.d( "WORK","YES");
            }else{
                Log.d( "WORK","NO");
                Intent i2 = new Intent(getBaseContext(), Checker.class);
                startForegroundService(i2);
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager)context. getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service already","running");
                return true;
            }
        }
        Log.i("Service not","running");
        return false;
    }

}
